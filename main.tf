terraform {
  required_providers {
    postgresql = {
      source = "cyrilgdn/postgresql"
      version = "1.20.0"
    }
  }
}

provider "kubernetes" {
  config_path    = "/etc/rancher/k3s/k3s.yaml"
}

provider "postgresql" {
  host            = kubernetes_service_v1.postgresql_service.spec[0].cluster_ip
  port            = 5432
  database        = "postgres"
  username        = "postgres"
  password        = var.postgresql_pass
  sslmode         = "disable"
  connect_timeout = 15
}

resource "local_file" "ansible-config-yaml" {
    filename="${path.module}/ansible-variables.yaml"
    content=<<-EOT
        cyverse_pass: ${ var.cyverse_pass }
        cyverse_user: ${ var.cyverse_user }
        cyverse_mount_dir: ${ var.cyverse_mount_dir }
        webdav_host: ${ var.webdav_host }
        ml_artifact_dir: ${ var.ml_artifact_dir }
        cyverse_artifact_store_enable: ${ var.cyverse_artifact_store_enable }
    EOT
}

resource "null_resource" "ansible-execution" {
    triggers = {
        always_run = "${timestamp()}"
    }

    provisioner "local-exec" {
        command = "sudo pip3 install --upgrade pip && sudo pip3 install --upgrade ansible-core~=2.13"
    }

    provisioner "local-exec" {
        command = "ansible-galaxy install -r requirements.yaml -f"
        working_dir = "${path.module}"
    }

    provisioner "local-exec" {
        command = "ANSIBLE_HOST_KEY_CHECKING=False ANSIBLE_SSH_PIPELINING=True ANSIBLE_CONFIG=ansible.cfg ansible-playbook --forks=10 -e @ansible-variables.yaml playbook.yaml"
        working_dir = "${path.module}"
    }
}


resource "kubernetes_deployment_v1" "postgresql" {
    metadata {
        name = "postgres-deployment"
        labels = {
            app = "postgres"
        }
    }

    spec {
        replicas = 1
        selector {
          match_labels = {
            app = "postgres"
          }
        }
        template {
          metadata {
            labels = {
              app = "postgres"
            }
          }
          spec {
            container {

              image = "postgres:alpine"
              name = "postgres"

              port {
                container_port = 5432
              }

              env {
                name = "POSTGRES_PASSWORD"
                value = "${var.postgresql_pass}"
              }
            }
          }
        }
    }
}

resource "kubernetes_service_v1" "postgresql_service" {
    metadata {
        name = "postgresql-svc"
    }
    spec {
      selector = {
        app = "postgres"
      }
      port {
        port = 5432
        target_port = 5432
      }
    }
}

locals {
  artifact_store_text = var.cyverse_artifact_store_enable ? "The CyVerse Data Store" : "local storage"
}

# postgresql_database.mlflowdb needs to connect to the postgresql service
# why do you think we need this?
resource "time_sleep" "wait_5s_seconds" {
  depends_on = [kubernetes_service_v1.postgresql_service, kubernetes_deployment_v1.postgresql]
  create_duration = "5s"
}

resource "postgresql_database" "mlflowdb" {
  name              = "mlflowdb"
  depends_on = [time_sleep.wait_5s_seconds]
}

resource "null_resource" "mlflow-server" {
    triggers = {
        always_run = "${timestamp()}"
        port_for_kill = var.mlflow_port
    }
  depends_on = [postgresql_database.mlflowdb]
    provisioner "local-exec" {
        command = "sudo pip3 install --upgrade pip && sudo pip3 install --ignore-installed mlflow psycopg2"
    }

provisioner "local-exec" {
    interpreter = ["bash", "-c"]
    command = <<EOL
    mlflow server \
      --backend-store-uri postgresql://postgres:${var.postgresql_pass}@${kubernetes_service_v1.postgresql_service.spec[0].cluster_ip}/mlflowdb \
      --default-artifact-root=file://${var.ml_artifact_dir} \
      --artifacts-destination=file://${var.ml_artifact_dir} \
      --port ${var.mlflow_port} \
      > mlflow.log 2>&1 &
    EOL
}

provisioner "local-exec"{
  when = destroy
  command = "fuser -k ${self.triggers.port_for_kill}/tcp"
}

}


output "mlops_help_text" {
    value = <<-EOT

        Your MLFlow artifact store is configured to be: ${local.artifact_store_text}
        The location of your artifact store is: ${var.ml_artifact_dir}
        Use this as your MLFlow connection string:
        
            postgresql://postgres:${var.postgresql_pass}@${kubernetes_service_v1.postgresql_service.spec[0].cluster_ip}/mlflowdb
        Use this to access the MLFlow server:
            https://localhost:${var.mlflow_port}
    EOT
}