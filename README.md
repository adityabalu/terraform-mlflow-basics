# terraform-mlflow-basics

This terraform will setup basic dependencies for CyVerse Cloud Native Camp's MLFlow tutorial in collaboration with the [Translational AI Center](https://trac-ai.iastate.edu/).

Disclaimer: DO NOT USE THIS TERRAFORM FOR PRODUCTION PURPOSES

Minimally, the code sets up either a local artifact store or mounts the CyVerse Data Store as a remote artifact store, using [WebDAV](https://learning.cyverse.org/ds/webdav/).

Postgresql will also be installed into kubernetes with a password, which can be used for your mlflow tracking backend.

## Requirements

1. terraform, installation instructions can be found here [https://developer.hashicorp.com/terraform/downloads](https://developer.hashicorp.com/terraform/downloads)
2. a vm with kubernetes e.g. use CACAO k3s template
3. mlflow, instructions forthcoming

## Using this repo

1. ssh into the instance with k3s (or the head node of kubernetes)
2. `git clone https://gitlab.com/stack0/terraform-mlflow-basics.git`
3. `cd terraform-mlflow-basics`
4. `cp terraform.tfvars.example-cyverse terraform.tfvars`
5. `vi terraform.tfvars # update your cyverse username and password here`
6. `terraform init`
7. `terraform apply -auto-approve`

If all goes well, you should see an output similar to the following:
```bash
Your MLFlow artifact store is configured to be: The CyVerse Data Store
The location of your artifact store is: /mnt/cyverse/mlruns
Use this as your MLFlow connection string:
        
    postgresql://postgres:my-postgresql-secret-1234@10.0.0.0/mlflowdb

EOT
```
Note, you can change the default postgresql password, but this setup is only for demo/tutorial purposes and should never be used in a production setting.

Here's a table of all the input variables that can be used in `terraform.tfvars`
| variable | description | default |
|----------|-------------|----------|
| `cyverse_user` | your cyverse username | none |
| `cyverse_pass` | your cyverse password | none |
| `webdav_host` | webdav host (for cyverse ) | data.cyverse.org |
| `cyverse_mount_dir` | path on system to mount cyverse | `/mnt/cyverse` |
| `ml_artifact_dir` | path on system for artifact store | `/mnt/cyverse/mlruns` |
| `cyverse_artifact_store_enable` | if `true`, enable cyverse-backed artifact store; if `false`, use local filesystem | `true` |
| `postgresql_pass` | default password for postgresql; do not ever expose the postgresql port externally | `my-postgresql-secret-1234` |