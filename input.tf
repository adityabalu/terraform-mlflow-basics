variable "cyverse_user" {
  type = string
  description = "string, irods username"
  default = ""
}

variable "cyverse_pass" {
    type = string
    description = "string, irods password"
    sensitive = true
    default = ""
}

variable "webdav_host" {
    type = string
    description = "string, host"
    default = "data.cyverse.org"
}

variable "cyverse_mount_dir" {
  type = string
  description = "string, the directory where to mount the webdav host"
  default = "/mnt/cyverse"
}

variable "cyverse_artifact_store_enable" {
  type = bool
  description = "string, if true will enable the cyverse artifact store, otherwise local disk will be used"
  default = true
}

variable "ml_artifact_dir" {
  type = string
  description = "string, the local directory to store the ml artifacts"
  default = "/mnt/cyverse/mlruns"
}

variable "postgresql_pass" {
  type = string
  description = "string, password to pass into the k8s postgresql deployment; NOT FOR A PRODUCTION ENVIRONMENT"
  default = "my-postgresql-secret-1234"
}

variable "mlflow_port" {
  type = string
  description = "int, port to be used by mlflow for deployment; NOT FOR A PRODUCTION ENVIRONMENT"
  default = "5005"
}